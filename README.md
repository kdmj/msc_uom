## Discription of content
---

1. [Data processing](/Data%20processing/) : Data pre-processing. 
    
    1. [Feature_Creator_Thesis_Work.ipynb](/Data%20processing/Feature_Creator_Thesis_Work.ipynb) : Dedicated grapsh 
    
    2. [Haptic_dataset_creator.ipynb](/Data%20processing/Haptic_dataset_creator.ipynb) : Preprocessing the data and create sets.

    3. [Applying_Multiple_ML_models](/Data%20processing/multiple_ML_model_training.ipynb) : Trying multiple machine learning models identify best performing ML models and best performing feature set.

    4. [Plot_variation.ipynb](/Data%20processing/Plot_Variation.ipynb) : Crate low resolution dataset and plot those

    5. [PlotVariations2.ipynb](/Data%20processing/PlotVariation2.ipynb) : Crate low resolutin data and plot those

    6. [Ploting_data.ipynb](/Data%20processing/Ploting_data.ipynb) : Actual creator of the of the haptic_data_*.ts

2. [Camera](/camera/) : Video and image capturing from raspberry pi

    1. [takepic](/camera/takepic.py) : Take 1 image
    2. [recording_vid](/camera/recording_vid.py) : capruting video 

3. [Sensoray826](/Sensoray826/) is a final stage deverlpmne of the MSc-UoM github repo. For full deverlopment visit [github.com/malithjkd/MSc-UoM](https://github.com/malithjkd/MSc-UoM)


--- 
## Installation
---
Program ran on separate python environment. 

Use following command to create python envirmnet using envirmnet file. 

``` consol
conda env create --file bio-env.txt 
```


---
## Supporing documents.
---
Conda [Cheet Sheet](https://docs.conda.io/projects/conda/en/4.6.0/_downloads/52a95608c49671267e40c689e0bc00ca/conda-cheatsheet.pdf)

numpy 

pandas

matplotlib

---

