\beamer@sectionintoc {1}{Table of content}{2}{0}{1}
\beamer@sectionintoc {2}{Introduction}{3}{0}{2}
\beamer@subsectionintoc {2}{1}{Evolution / Development}{3}{0}{2}
\beamer@subsectionintoc {2}{2}{Haptics}{4}{0}{2}
\beamer@subsectionintoc {2}{3}{Applications}{5}{0}{2}
\beamer@subsectionintoc {2}{4}{Observer Technology}{6}{0}{2}
\beamer@sectionintoc {3}{Haptic Object Identification}{9}{0}{3}
\beamer@subsectionintoc {3}{1}{Basic}{9}{0}{3}
\beamer@subsectionintoc {3}{2}{Surface Texture}{10}{0}{3}
\beamer@subsectionintoc {3}{3}{Shape}{13}{0}{3}
\beamer@subsectionintoc {3}{4}{Vibration}{15}{0}{3}
\beamer@subsectionintoc {3}{5}{Deformation}{17}{0}{3}
\beamer@subsectionintoc {3}{6}{Summary}{20}{0}{3}
\beamer@sectionintoc {4}{Objectives}{21}{0}{4}
\beamer@sectionintoc {5}{Methodology}{22}{0}{5}
\beamer@sectionintoc {6}{Work done}{23}{0}{6}
\beamer@sectionintoc {7}{Future work}{24}{0}{7}
\beamer@sectionintoc {8}{Publications}{25}{0}{8}
\beamer@sectionintoc {9}{References}{26}{0}{9}
