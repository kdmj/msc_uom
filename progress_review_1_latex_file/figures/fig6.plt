set terminal latex size 3.1,2.5
set output 'fig6.tex'

set yrange [0:500]
set style data histogram
set style histogram cluster gap 1
set style fill solid
set ylabel "Quantity"
set xlabel "Year"
set boxwidth 0.5
set xtics format ""
set grid ytics

set title "Automated system / Robot Annual Installs (x1000)"

plot "fig6data.dat" using 2:xtic(1) title "Total installation"
