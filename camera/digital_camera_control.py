import time						# to activate sleep fuction
import picamera						# for camera operation
import RPi.GPIO as GPIO					# to facilitate ditigal input
#from datetime import datetime				# to create file names(if required)

camera = picamera.PiCamera()

camera.rotation = 180
camera.resolution = (1269, 972)

GPIO.setmode(GPIO.BOARD)
GPIO.setup(16, GPIO.IN)

state = 0
previous_state = 0
i = 0

while True:
	state = GPIO.input(16)
	if (state == 1 and previous_state == 0):
		camera.capture('1_1_1.jpg')		# to capture starting photo
		camera.start_recording('1_1_3.h264')	# start recoding vid name
		camera.wait_recording(23)		# recodeing wait time (time seconds)
		camera.stop_recording()			# stop recording

	if (state == 0 and previous_state == 1):
		camera.capture('1_1_2.jpg')		# to capture end photo

	time.sleep(0.5)
	previous_state = state
