#malithjkd
#12.09.2020
#to capture the vid strem from the raspberry pi
#documentation https://picamera.readthedocs.io/en/release-1.13/index.html

import picamera
from datetime import datetime
import time

camera = picamera.PiCamera()
date = datetime.now()

years = "%04d" % date.year
months = "%02d" % date.month
dates = "%02d" % date.day
hours = "%02d" % date.hour
mint = "%02d" % date.minute
sec = "%02d" % date.second
vid_name = str(years)+str(months)+str(dates)+str(hours)+str(mint)+str(sec)+'.h264'	# extention .mjpeg or .h264

camera.rotation = 180
#camera.resolution = (2592, 1944)
camera.resolution = (1269, 972)
#camera.resolution = (640, 480)

#camera.start_preview()
#camera.start_recording(vid_name)
camera.start_recording('testvid.h264')
camera.wait_recording(30)
camera.stop_recording()
time.sleep(1)
#camera.stop_preview()
