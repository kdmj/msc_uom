# This is HOW TO guide for raspberry pi operation

## Remote access to raspberry pi (SSH) 

To connect

```console
user@user:~$ ssh pi@10.8.122.2
password: raspberry*
```

If password is correct 
```console
pi@raspberrypi:~ $ 
```


Go to file location 

```console
pi@raspberrypi:~ $ cd Documents/MSc_UoM/Camara
```

To run the preview of the camara 

```console
pi@raspberrypi:~/Documents/msc_uom/camera $ python preview_vid.py
```

To take pictures
```console
pi@raspberrypi:~/Documents/msc_uom/camera $ python takepic.py
```

## File sharing

Secure copy protocol(SCP)

```consol
scp pi@<ipOfTheRaspberrpi>:<fileparth>
```

```consol
scp pi@10.8.122.3:Docments/msc_uom/camera/<filename>
```


---

## Setting up static IP address for raspberry pi 

|Description |#   |
|:-----|:--------|
|IP|10.8.121.102|
|Gate|10.8.121.254|
|Sub|255.225.225.0|
|DNS|192.248.8.97|

To config it to raspberry pi edit dhcpcd.conf file

```console
pi@raspberry: sudo nano /ect/dhcpcd.conf
```

then edit the file as following 

```
interface eth0

static ip_addres = 10.8.121.102/24	

static routers = 10.8.121.254

static domain_name_servers = 192.248.8.97
```

then ``` ctrl+x``` to exit

to save existing file ``` Y ``` and enter

