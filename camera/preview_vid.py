#malithjkd
#12.09.2020
#to capture the vid strem from the raspberry pi
#documentation https://picamera.readthedocs.io/en/release-1.13/index.html

import picamera
from datetime import datetime
import time

camera = picamera.PiCamera()

camera.rotation = 180
camera.resolution = (2592, 1944)
#camera.resolution = (1269, 972)
#camera.resolution = (640, 480)
camera.start_preview()
time.sleep(50)
camera.stop_preview()
