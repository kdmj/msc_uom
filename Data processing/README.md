## Discription of content
---
1. [Feature_Creator_Thesis_Work.ipynb](/Data%20processing/Feature_Creator_Thesis_Work.ipynb) : Dedicated grapsh 
    
2. [Haptic_dataset_creator.ipynb](/Data%20processing/Haptic_dataset_creator.ipynb) : Preprocessing the data and create sets.

3. [Applying_Multiple_ML_models](/Data%20processing/multiple_ML_model_training.ipynb) : Trying multiple machine learning models identify best performing ML models and best performing feature set.

4. [Plot_variation.ipynb](/Data%20processing/Plot_Variation.ipynb) : Crate low resolution dataset and plot those

5. [PlotVariations2.ipynb](/Data%20processing/PlotVariation2.ipynb) : Crate low resolutin data and plot those

6. [Ploting_data.ipynb](/Data%20processing/Ploting_data.ipynb) : Actual creator of the of the haptic_data_*.ts

---
## Usage
---

1. Pro-process raw data from the setup.

2. Create plots for thesis and papers. 

3. Create datasets for ML model training. 

---
## Dataset History
---

|Number|Dataset name    |Objects                              |Dimention|Note   |
|:----:|:--------------:|:-----------------------------------:|:-------:|:-----:|
|1     |haptic_data_1.ts|Banana, Onion, Orange, Sponge, Tomato| 62,4    |Test and train data|
|2     |haptic_data_2.ts|Banana, Onion, Orange, Sponge, Tomato| 62,9    |Test and train data|
|3     |haptic_data_3.ts|Orange, Tomato                       | 156,4   |Train data(without features)|
|4     |haptic_data_4.ts|Orange, Tomato                       | 63,4    |Test data(without features)|
|5     |haptic_data_5.ts|Orange, Tomato                       | 158,9   |Train data|
|6     |haptic_data_6.ts|Orange, Tomato                       | 63,9    |Test data|
|8     |haptic_data_7.ts|Orange, Tomato                       | 157,9   |Train data|
|9     |haptic_data_8.ts|Orange, Tomato                       | 59,9    |Test data|


---
## File opearations with raspberry pi 
---

### Sending files to raspberry pi 

```console
scp FileName.txt pi@10.10.11.44:/home/pi/Documents
```

### coping from raspberry pi 
```console
scp pi@10.10.11.44:/home/pi/Documents/test.txt /home/linuxlite/Documents/msc_uom/
```

---
## Remote server connection
---

To activate conda base enviroment

```console
conda activate base
```

to see the evriroment list

```console
conda env list
```

to see which python is running

```console
which python
```

One time work

Created password folder for jupyter notebook 

```console
mkdir .jupyter
```

Create password for jupyter notebook

```console
jupyter notebook password

Ele@hap7!c

Ele@hap7!c
```

to start jupyter notbook server without browser and specific port

```console
jupyter lab --no-browser --port=9996
```


### Local pc setup for Jupyter notebook setup
---

Connect local pc localhost to server localhost

```console
ssh -N -f -L localhost:9006:localhost:9996 haptic@haptic.elect.projects.uom.lk
```

Then start browser to give url

```console
localhost:9006
```

jupyter notebook should work now!!!


---
## Files oparations: Copy from local pc to server
---

go to the file location 

```console
$ cd

$ scp 1_12_1_0.txt haptic@haptic.elect.projects.uom.lk:malith/
```
