#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x7799aa3d, "module_layout" },
	{ 0x3ec8886f, "param_ops_int" },
	{ 0xe9fc3117, "pci_unregister_driver" },
	{ 0x6bc3fbc0, "__unregister_chrdev" },
	{ 0x65a3da7e, "class_destroy" },
	{ 0x941eacf6, "__pci_register_driver" },
	{ 0xfec088ad, "__class_create" },
	{ 0xea41f249, "__register_chrdev" },
	{ 0x1000e51, "schedule" },
	{ 0xfa66f77c, "finish_wait" },
	{ 0xd62c833f, "schedule_timeout" },
	{ 0x5c8b5ce8, "prepare_to_wait" },
	{ 0xc8b57c27, "autoremove_wake_function" },
	{ 0xe2929302, "current_task" },
	{ 0xb54533f7, "usecs_to_jiffies" },
	{ 0x4f8b5ddb, "_copy_to_user" },
	{ 0x4f6b400b, "_copy_from_user" },
	{ 0xa1c76e0a, "_cond_resched" },
	{ 0xcf21d241, "__wake_up" },
	{ 0x8f64aa4, "_raw_spin_unlock_irqrestore" },
	{ 0x9327f5ce, "_raw_spin_lock_irqsave" },
	{ 0x2072ee9b, "request_threaded_irq" },
	{ 0xecea79ab, "device_create" },
	{ 0x9ed5f8e8, "cdev_add" },
	{ 0xf9dc1554, "cdev_init" },
	{ 0xabe4da1, "cdev_alloc" },
	{ 0x7ab547dc, "dev_set_drvdata" },
	{ 0x42c8de35, "ioremap_nocache" },
	{ 0x1fedf0f4, "__request_region" },
	{ 0x2e4b174, "dma_set_mask" },
	{ 0xbccbcaca, "pci_set_master" },
	{ 0x7ad7cbaa, "pci_enable_device" },
	{ 0xc695b60e, "__mutex_init" },
	{ 0x6395be94, "__init_waitqueue_head" },
	{ 0x45e82b95, "remap_pfn_range" },
	{ 0x964bcb2, "boot_cpu_data" },
	{ 0x27e1a049, "printk" },
	{ 0x86bfe20c, "kmem_cache_alloc_trace" },
	{ 0x972d523a, "kmalloc_caches" },
	{ 0xd22651e6, "mutex_unlock" },
	{ 0x39ab2b2d, "mutex_lock" },
	{ 0x7c61340c, "__release_region" },
	{ 0x69a358a6, "iomem_resource" },
	{ 0x80e06862, "pci_disable_device" },
	{ 0x37a0cba, "kfree" },
	{ 0x93b4bd0a, "cdev_del" },
	{ 0x901fdb5b, "device_destroy" },
	{ 0xedc03953, "iounmap" },
	{ 0xf20dabd8, "free_irq" },
	{ 0xfe74a693, "dev_get_drvdata" },
	{ 0xb4390f9a, "mcount" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("pci:v000010B5d00009056sv00006000sd00000826bc*sc*i*");

MODULE_INFO(srcversion, "39309B1A67E27043B6D4E05");
