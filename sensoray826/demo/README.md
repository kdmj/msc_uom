
# Force controller 

## Disturbance observer
--- 


![](https://ars.els-cdn.com/content/image/3-s2.0-B9780128211946000032-f03-09-9780128211946.sml) 

source : https://doi.org/10.1016/B978-0-12-821194-6.00003-2

---
## Reaction force observer
---
![](https://ars.els-cdn.com/content/image/3-s2.0-B9780128211946000032-f03-14-9780128211946.jpg)

source : https://doi.org/10.1016/B978-0-12-821194-6.00003-2

---


