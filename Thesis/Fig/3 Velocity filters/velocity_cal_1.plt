#set timestamp
set terminal pdf
#set output "velocity_dif.pdf"
set output "velocity_sg_filter.pdf"
set title "Time vs Velocity"
set xlabel "Time (s)"
set ylabel "Velocity(mm/s)
set yrange [0:2]
set xrange [0:3]
set datafile separator ','

### To create specific line color
set style line 1 lc rgb "#0390fc"
set style line 2 lc rgb "#43b580"
set style line 3 lc rgb "#b58d43"

plot "features_matrix_1.csv" using 2:6 with line ls 1  title "g_{fil} = 3", "features_matrix_1.csv" using 2:7 with line ls 2  title "g_{fil} = 10", "features_matrix_1.csv" using 2:8 with line ls 3  title "g_{fil} = 100"





#from txt file
# 1 Time
# 2 Force input
# 3 Force output
# 4 Position

#From csv file
# 2 Time
# 3 Force input
# 4 Force output
# 5 Position
# 6 Velocity g 3
# 7 Velocity g 10
# 8 Velocity g 100
# 9 Acceleration
# 10 velocity1

#plot "2_12_0_0.txt" using ($4*0.000005):($2) with line title "Force position object1"
#plot "features_metrix_1.csv" using 2:4 with line title "Force reaction"
#plot "features_metrix_1.csv" using ($0):($5) with line title "Position"
#plot "features_matrix_1.csv" using 2:8 with line title "Object 1"

#set style line 5 lt rgb "#0390fc" lw 0.2 pt 0.2
#plot "features_matrix_1.csv" using 2:6 with linespoints ls 5  title "Object 1"

